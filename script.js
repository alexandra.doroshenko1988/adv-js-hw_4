// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.


// AJAX дуже корисний, оскільки він дозволяє взаємодіяти з сервером без необхідності повного оновлення сторінки. Це дає можливість створювати більш динамічні та інтерактивні веб - сторінки, які можуть змінюватися без втрати даних, що були введені користувачем.





const filmsList = document.getElementById("filmsList");

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((data) => {
    data.forEach((film) => {
      const filmItem = document.createElement("div");
      filmItem.classList.add("film-item");

      const title = document.createElement("h2");
      title.textContent = `Episode ${film.episodeId}: ${film.director}`;
      filmItem.appendChild(title);

      const openingCrawl = document.createElement("p");
      openingCrawl.textContent = film.opening_crawl;
      filmItem.appendChild(openingCrawl);

      const charactersList = document.createElement("ul");
      film.characters.forEach((characterURL) => {
        fetch(characterURL)
          .then((response) => response.json())
          .then((character) => {
            const characterName = character.name;
            const characterItem = document.createElement("li");
            characterItem.textContent = characterName;
            charactersList.appendChild(characterItem);
          });
      });

      filmItem.appendChild(charactersList);
      filmsList.appendChild(filmItem);
    });
  })
  .catch((error) => {
    console.error("An error occurred:", error);
  });
